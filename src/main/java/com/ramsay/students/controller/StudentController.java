package com.ramsay.students.controller;

import com.ramsay.students.entity.Student;
import com.ramsay.students.service.StudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(originPatterns = "http://localhost:*")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String start() {
        return "Welcome to Students service";
    }

    @RequestMapping(value = "student", method = RequestMethod.GET)
    public List<Student> readStudents() {
        return studentService.readStudents();
    }

    @RequestMapping(value = "student", method = RequestMethod.POST)
    public Object createStudent(@RequestBody Student student) {
        return studentService.createStudent(student);
    }

    @RequestMapping(value = "student", method = RequestMethod.PUT)
    public String updateStudent(@RequestBody Student student) {
        return studentService.updateStudent(student);
    }

    @RequestMapping(value = "student/{id}", method = RequestMethod.DELETE)
    public String deleteStudent(@PathVariable String id) {
        return studentService.deleteStudent(Integer.parseInt(id));
    }
}
