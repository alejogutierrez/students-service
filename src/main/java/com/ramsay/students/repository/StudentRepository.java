package com.ramsay.students.repository;

import com.ramsay.students.entity.Student;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Student, Integer> {
    public boolean existsByUsername(String username);
    public boolean existsById(int id);
    public Student findById(int id);

    @Query("select max(s.id) from Student s")
    public Integer findMaxId();
}
