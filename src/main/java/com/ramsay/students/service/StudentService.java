package com.ramsay.students.service;

import com.ramsay.students.entity.Student;
import com.ramsay.students.repository.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;

    @Transactional
    public Object createStudent(Student student) {
        try {
            if (!studentRepository.existsByUsername(student.getUsername())) {
                student.setId(null == studentRepository.findMaxId()? 0 : studentRepository.findMaxId() + 1);
                return studentRepository.save(student);
            } else {
                return "Student already exists in the database.";
            }
        } catch (Exception e) {
            throw e;
        }
    }

    public List<Student> readStudents() {
        return studentRepository.findAll();
    }

    @Transactional
    public String updateStudent(Student student) {
        if (studentRepository.existsById(student.getId())) {
            try {
                Student studentToBeUpdate = studentRepository.findById(student.getId());
                studentToBeUpdate.setUsername(student.getUsername());
                studentToBeUpdate.setFirstName(student.getFirstName());
                studentToBeUpdate.setLastName(student.getLastName());
                studentToBeUpdate.setAge(student.getAge());
                studentToBeUpdate.setCareer(student.getCareer());
                studentRepository.save(studentToBeUpdate);
                return "Student record updated.";
            } catch (Exception e) {
                throw e;
            }
        } else {
            return "Student does not exists in the database.";
        }
    }

    @Transactional
    public String deleteStudent(int id) {
        if (studentRepository.existsById(id)) {
            try {
                Student toDelete = studentRepository.findById(id);
                studentRepository.delete(toDelete);
                return "Student record deleted successfully.";
            } catch (Exception e) {
                throw e;
            }
        } else {
            return "Student does not exist";
        }
    }
}
