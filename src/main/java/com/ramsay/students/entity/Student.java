package com.ramsay.students.entity;

import javax.persistence.Column;
import  javax.persistence.Entity;
import  javax.persistence.Id;

@Entity
public class Student {
    @Id
    private int id;

    @Column( name = "username")
    private String username;

    @Column( name = "firstname")
    private String firstName;

    @Column( name = "lastname")
    private String lastName;

    @Column( name = "age")
    private int age;

    @Column( name = "career")
    private String career;

    public Student() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getCareer() {
        return career;
    }

    public void setCareer(String career) {
        this.career = career;
    }
}
